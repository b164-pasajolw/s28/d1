//Javascript Synchronous vs. Asynchronous

//Synchronous Javascript
//Javascript is by default is synchronous meaning that only one statement run/execute at a time

/*
console.log("Hello World");

console.log("Goodbye");

for(let i = 0; i <= 100; i++){
	console.log(i)
}

//blocking, is just a slow process of code.
console.log("Hello World")

//Asynchronous Javascript
//not occuring at the same time

function printMe() {
	console.log('print me')
};

//printMe();
function test() {
	console.log('test')
};

setTimeout(printMe, 5000); //delaying the printOut
test();
*/

//The Fetch API allows you to asynchronously request for a resource (data)
//the fetch receives a PROMISE
//A 'Promise' is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value

//fetch() is a method in JS, which allows us to send a request to an API and process its response

//fetch(url, {options}).then(response => response.json()).then(data => {console.log(data)})

//url = > the url to resource/routes from the Server
//optional objects = > it contains additional information about our requests such as method, the bosy and the headers. 

//it parse the response as JSON
//process the results

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

//A promise may be in one of 3 possible states: fulfilled, rejected or pending


//Retrieves all post (GET)

fetch('https://jsonplaceholder.typicode.com/posts',{
	method:'GET'
})
.then(response => response.json())
.then(data => {
	console.log(data)
})

//.then method captures the response object and returns another "promise" which will eventually be "resolved" or "rejected".


//"async" and "await" keywords is another approach that can be used to achieve asynchronuos code
//used in functions to indicate which portions of code should be waited for 

async function fetchData(){

	let result = await fetch ('https://jsonplaceholder.typicode.com/posts');

	//Result returned by fetch
	console.log(result);

	console.log(typeof result);

	console.log(result.body);

	//Converts data from the response object as JSON
	let json = await result.json();

	console.log(json);

}

fetchData()


//Creating a post
//Creating a new post following the Rest API (create, POST)
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello World',
		iserId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


//Updating a post

//Update a specific post following the Rest API (update, PUT)
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method:'PATCH',
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		//id:1,
		title: "Updated post"
		//userId:1
	})
})
.then(res => res.json())
.then(data => console.log(data))

//The PUT is a method modifying resources where the client sends data that updates the ENTIRE resources. It is used to set an entity's information completely
//Patch method applies a partial update to the resources

//Delete a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))

//isActive:false

//Filtering post
//THe data can be filtered by sending the userId along with the URL
//? symbol at the end of the URL indicates that parameters will be sent to the endpoint
//Syntax:
//Individual Parameters
	//'url?parameterName=value'
//Multiple Parameters
	//'url?parameterNameA=valueA&paramB=valueB'

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(res => res.json())
.then(data => console.log(data))


//Retrieving nested/related data

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then(res => res.json())
.then(data => console.log(data))

